/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import lat1.ButtonHandler;
import lat4.Ch14JButtonFrame;

public class tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JTextField bilangan1;
    private JTextField bilangan2;
    private JTextField hasil;
    private JLabel bil1;
    private JLabel bil2;
    private JLabel hsl;
    private JButton button;
    private int a, b, hasill;
    private Container content = getContentPane();

    public static void main(String[] args) {
        tugas1 frame = new tugas1();
        frame.setVisible(true);

    }

    public tugas1() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        setLayout(null);

        bil1 = new JLabel("bilangan 1");
        bil1.setBounds(10, 10, 70, 30);
        contentPane.add(bil1);

        bilangan1 = new JTextField();
        bilangan1.setBounds(80, 15, 80, 20);
        contentPane.add(bilangan1);
//
        bil2 = new JLabel("bilangan 2");
        bil2.setBounds(10, 50, 70, 30);
        contentPane.add(bil2);

        bilangan2 = new JTextField();
        bilangan2.setBounds(80, 55, 80, 20);
        contentPane.add(bilangan2);
//
        hsl = new JLabel("hasil");
        hsl.setBounds(10, 90, 70, 30);
        contentPane.add(hsl);

        hasil = new JTextField();
        hasil.setBounds(80, 95, 80, 20);
        contentPane.add(hasil);

        button = new JButton("Jumlah");
        button.setBounds(80, 150, 80, 40);
        button.addActionListener(this);
        contentPane.add(button);

        ButtonHandler handler = new ButtonHandler() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                a = Integer.parseInt(bilangan1.getText());
                b = Integer.parseInt(bilangan2.getText());
                hasill = a + b;
                hasil.setText(Integer.toString(hasill));
            }
        };
        button.addActionListener(handler);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
