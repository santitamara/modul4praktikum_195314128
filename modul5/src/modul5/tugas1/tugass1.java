/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul5.tugas1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class tugass1 extends JFrame {

    JPanel panel;
    JTextField panjang, lebar, Luas;
    JButton Hitung;

    public tugass1() {
        setSize(200, 200);
        setTitle("Luas Tanah");
        setLocation(100, 100);
        setResizable(false);
        panel = new JPanel();

        JLabel labelPanjang = new JLabel("Panjang(m)");
        panjang = new JTextField(10);

        JLabel labelLebar = new JLabel("Lebar(m)");
        lebar = new JTextField(10);

        JLabel labelLuas = new JLabel("Luas(m2)");
        Luas = new JTextField(10);
        Luas.setEditable(false);

        Hitung = new JButton("Hitung");
        Hitung.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                try {
                    int Panjang = Integer.parseInt(panjang.getText());
                    int Lebar = Integer.parseInt(lebar.getText());
                    int luas = Panjang * Lebar;
                    String hasil = Integer.toString(luas);
                    Luas.setText(hasil);
                    panjang.setText(null);
                    lebar.setText(null);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Maaf, Hanya Integer yang diperbolehkan!");
                    panjang.setText(null);
                    lebar.setText(null);
                }
            }

        });

        panel.add(labelPanjang);
        panel.add(panjang);
        panel.add(labelLebar);
        panel.add(lebar);
        panel.add(labelLuas);
        panel.add(Luas);
        panel.add(Hitung);
        this.add(panel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        tugass1 main = new tugass1();
        main.setVisible(true);
    }
}
